# **My UBER**
 
This is UBER-like service for freight trucks.
This service can help regular people to deliver their stuff and help drivers to find loads and earn some money.
___
To run this project locally at first you need to clone this repository to your computer.<br/>
Then open the command line, and run these three commands:<br/> 
**cd nodejs-homework3** (to go to the project folder)<br/>
**npm install** (to download dependencies)<br/>
**npm run start** (to run the project on localhost).<br/> 
Now you can send requests at the URL **http://localhost:8080/**