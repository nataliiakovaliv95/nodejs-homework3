const User = require('../models/user');
const {signToken} = require('../middlewares/tokenMiddleware');
const {cryptPassword} = require('../services/passwordService');

const registerUser = async (req, res) => {
  const {email, password, role} = req.body;
  try {
    const newUser = new User({
      email,
      password: await cryptPassword(password),
      role,
    });
    await User.addUser(newUser);
    res.status(200).json({message: 'Profile created successfully'});
  } catch (err) {
    res.status(500).json({message: 'Server error'});
    console.log(err);
  }
};

const loginUser = async (req, res) => {
  const {email} = req.body;
  try {
    const user = await User.getUserByEmail(email);
    const token = signToken(user.toJSON());
    res.status(200).json({jwt_token: token});
  } catch (err) {
    res.status(500).json({message: 'Server error'});
    console.log(err);
  }
};

const resetUsersPassword = (req, res) => {
  res.status(200).json({
    message: 'New password sent to your email address',
  });
};

module.exports = {
  registerUser,
  loginUser,
  resetUsersPassword,
};
