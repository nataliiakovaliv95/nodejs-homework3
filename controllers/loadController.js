const Load = require('../models/load');
const Truck = require('../models/truck');
const {addNewLog} = require('../services/logService');
const {findFreeDriver} = require('../services/findDriverService');
const {loadStates} = require('../static/loadStates');
const {getNextLoadState} = require('../services/loadStateService');

const getLoads = async (req, res) => {
  const userId = req.decoded._id;
  const role = req.decoded.role;
  let {status, limit = 10, offset = 0} = req.query;
  limit = limit > 50 ? 50 : limit;
  try {
    if (role === 'DRIVER' && status === 'NEW') {
      return res.status(400).json({
        message: 'Driver can\'t have loads with status NEW'},
      );
    }
    const loads = await Load.getLoadsByFilter(
        userId, role, status, limit, offset,
    );
    res.status(200).json({loads});
  } catch (err) {
    res.status(500).json({message: 'Server error'});
    console.log(err);
  }
};

const addLoad = async (req, res) => {
  const userId = req.decoded._id;
  const loadData = req.body;
  try {
    const newLoad = new Load({
      created_by: userId,
      ...loadData,
    });
    await Load.saveLoad(newLoad);
    res.status(200).json({message: 'Load created successfully'});
  } catch (err) {
    res.status(500).json({message: 'Server error'});
    console.log(err);
  }
};

const getActiveLoad = async (req, res) => {
  const userId = req.decoded._id;
  try {
    const load = await Load.getActiveLoad(userId) || {};
    res.status(200).json({load});
  } catch (err) {
    res.status(500).json({message: 'Server error'});
    console.log(err);
  }
};

const updateLoadState = async (req, res) => {
  const userId = req.decoded._id;
  const role = req.decoded.role;
  try {
    const load = await Load.getActiveLoad(userId);
    if (!load) {
      return res.status(400).json({message: 'No active load found'});
    }
    const nextState = getNextLoadState(load.state);
    await Load.updateLoad(load._id, userId, role, {state: nextState});
    const message = `Load state changed to '${nextState}'`;
    await addNewLog(load, message);
    if (nextState === loadStates[loadStates.length - 1]) {
      await Load.updateLoad(load._id, userId, role, {status: 'SHIPPED'});
      await addNewLog(load, 'Load status changed to SHIPPED');
      await Truck.updateTruckStatus(userId, 'IS');
    }
    res.status(200).json({message});
  } catch (err) {
    res.status(500).json({message: 'Server error'});
    console.log(err);
  }
};

const getLoad = async (req, res) => {
  const userId = req.decoded._id;
  const {id} = req.params;
  try {
    const load = await Load.getLoad(id, userId);
    res.status(200).json({load});
  } catch (err) {
    res.status(500).json({message: 'Server error'});
    console.log(err);
  }
};

const updateLoad = async (req, res) => {
  const userId = req.decoded._id;
  const role = req.decoded.role;
  const {id} = req.params;
  const loadData = req.body;
  try {
    await Load.updateLoad(id, userId, role, loadData);
    res.status(200).json({message: 'Load details changed successfully'});
  } catch (err) {
    res.status(500).json({message: 'Server error'});
    console.log(err);
  }
};

const deleteLoad = async (req, res) => {
  const userId = req.decoded._id;
  const {id} = req.params;
  try {
    await Load.deleteLoad(id, userId);
    res.status(200).json({message: 'Load deleted successfully'});
  } catch (err) {
    res.status(500).json({message: 'Server error'});
    console.log(err);
  }
};

const postLoad = async (req, res) => {
  const userId = req.decoded._id;
  const role = req.decoded.role;
  const {id} = req.params;
  try {
    const load = await Load.updateLoad(id, userId, role, {status: 'POSTED'});
    await addNewLog(load, 'Load was successfully posted');
    const freeDriver = await findFreeDriver(load);
    if (!freeDriver) {
      const message = 'Driver was not found, load status changed to NEW';
      await Load.updateLoad(id, userId, role, {status: 'NEW'});
      await addNewLog(load, message);
      return res.status(400).json({message});
    }
    const driverId = freeDriver.created_by;
    const message = `Load assigned to driver with id ${driverId}`;
    await Truck.updateTruckStatus(driverId, 'OL');
    const loadData = {
      assigned_to: driverId,
      state: loadStates[0],
      status: 'ASSIGNED',
    };
    await Load.updateLoad(id, userId, role, loadData);
    await addNewLog(load, message);
    res.status(200).json({
      message: 'Load posted successfully',
      driver_found: true,
    });
  } catch (err) {
    res.status(500).json({message: 'Server error'});
    console.log(err);
  }
};

const getLoadShippingInfo = async (req, res) => {
  const userId = req.decoded._id;
  const {id} = req.params;
  try {
    const load = await Load.getLoad(id, userId);
    const truck = await Truck.getAssignedTruck(load.assigned_to);
    res.status(200).json({load, truck});
  } catch (err) {
    res.status(500).json({message: 'Server error'});
    console.log(err);
  }
};

module.exports = {
  getLoads,
  addLoad,
  getActiveLoad,
  updateLoadState,
  getLoad,
  updateLoad,
  deleteLoad,
  postLoad,
  getLoadShippingInfo,
};
