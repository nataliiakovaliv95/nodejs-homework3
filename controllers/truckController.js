const Truck = require('../models/truck');

const getTrucks = async (req, res) => {
  const userId = req.decoded._id;
  try {
    const trucks = await Truck.getTrucks(userId);
    res.status(200).json({trucks});
  } catch (err) {
    res.status(500).json({message: 'Server error'});
    console.log(err);
  }
};

const addTruck = async (req, res) => {
  const userId = req.decoded._id;
  const {type} = req.body;
  try {
    const newTruck = new Truck({
      created_by: userId,
      type,
    });
    await Truck.saveTruck(newTruck);
    res.status(200).json({message: 'Truck created successfully'});
  } catch (err) {
    res.status(500).json({message: 'Server error'});
    console.log(err);
  }
};

const getTruck = async (req, res) => {
  const userId = req.decoded._id;
  const {id} = req.params;
  try {
    const truck = await Truck.getTruck(id, userId);
    res.status(200).json({truck});
  } catch (err) {
    res.status(500).json({message: 'Server error'});
    console.log(err);
  }
};

const updateTruck = async (req, res) => {
  const userId = req.decoded._id;
  const {id} = req.params;
  const {type} = req.body;
  try {
    await Truck.updateTruck(id, userId, type);
    res.status(200).json({message: 'Truck details changed successfully'});
  } catch (err) {
    res.status(500).json({message: 'Server error'});
    console.log(err);
  }
};

const deleteTruck = async (req, res) => {
  const userId = req.decoded._id;
  const {id} = req.params;
  try {
    await Truck.deleteTruck(id, userId);
    res.status(200).json({message: 'Truck deleted successfully'});
  } catch (err) {
    res.status(500).json({message: 'Server error'});
    console.log(err);
  }
};

const assignTruck = async (req, res) => {
  const userId = req.decoded._id;
  const {id} = req.params;
  try {
    await Truck.assignTruck(id, userId);
    await Truck.updateTruckExceptSelected(id, userId);
    res.status(200).json({message: 'Truck assigned successfully'});
  } catch (err) {
    res.status(500).json({message: 'Server error'});
    console.log(err);
  }
};

module.exports = {
  getTrucks,
  addTruck,
  getTruck,
  updateTruck,
  deleteTruck,
  assignTruck,
};
