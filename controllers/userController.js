const User = require('../models/user');
const Truck = require('../models/truck');
const Load = require('../models/load');
const {cryptPassword} = require('../services/passwordService');

const getUserInfo = (req, res) => {
  const {_id, role, email, created_date} = req.decoded;
  res.status(200).json({user: {_id, role, email, created_date}});
};

const deleteUserProfile = async (req, res) => {
  const {_id, role} = req.decoded;
  try {
    await User.deleteUser(_id);
    if (role === 'DRIVER') {
      await Truck.deleteUsersTrucks(_id);
    }
    if (role === 'SHIPPER') {
      await Load.deleteUsersLoads(_id);
    }
    res.status(200).json({message: 'Profile deleted successfully'});
  } catch (err) {
    res.status(500).json({message: 'Server error'});
    console.log(err);
  }
};

const changeUserPassword = async (req, res) => {
  const {_id} = req.decoded;
  let {newPassword} = req.body;
  try {
    newPassword = await cryptPassword(newPassword);
    await User.changeUserPassword(_id, newPassword);
    res.status(200).json({message: 'Password changed successfully'});
  } catch (err) {
    res.status(500).json({message: 'Server error'});
    console.log(err);
  }
};

module.exports = {
  getUserInfo,
  deleteUserProfile,
  changeUserPassword,
};
