const checkRole = (correctRole) => {
  return (req, res, next) => {
    const {role} = req.decoded;
    if (role !== correctRole) {
      return res.status(400).json({
        message: 'You do not have permission to do this',
      });
    }
    next();
  };
};

const isDriver = checkRole('DRIVER');

const isShipper = checkRole('SHIPPER');

module.exports = {
  isDriver,
  isShipper,
};
