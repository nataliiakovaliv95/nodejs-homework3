const Joi = require('joi');
const User = require('../../models/user');

const registerSchema = Joi.object().keys({
  email: Joi.string().email().required(),
  password: Joi.string().required(),
  role: Joi.string().valid('SHIPPER', 'DRIVER').required(),
});

const loginSchema = Joi.object().keys({
  email: Joi.string().email().required(),
  password: Joi.string().required(),
});

const resetPasswordSchema = Joi.object().keys({
  email: Joi.string().email().required(),
});

const authValidator = async (req, res, next) => {
  const data = req.body;
  const url = req.url;
  try {
    switch (url) {
      case '/register':
        await registerSchema.validateAsync(data);
        next();
        break;
      case '/login':
        await loginSchema.validateAsync(data);
        next();
        break;
      case '/forgot_password':
        await resetPasswordSchema.validateAsync(data);
        next();
        break;
      default:
        next();
    }
  } catch (err) {
    const message = err.details[0].message;
    res.status(400).json({message});
  }
};

const isUserExist = async (req, res, next) => {
  const {email} = req.body;
  const url = req.url;
  try {
    let message;
    const user = await User.getUserByEmail(email);
    switch (true) {
      case user && url === '/register':
        message = 'You already have an account';
        break;
      case !user && (url === '/login' || url === '/forgot_password'):
        message = 'User not found';
        break;
    }
    if (message) {
      return res.status(400).json({message});
    }
    next();
  } catch (err) {
    res.status(500).json({message: 'Server error'});
    console.log(err);
  }
};

module.exports = {
  authValidator,
  isUserExist,
};
