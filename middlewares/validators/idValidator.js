const {Types} = require('mongoose');

const idValidator = async (req, res, next) => {
  const {id} = req.params;
  try {
    const correctId = Types.ObjectId.isValid(id);
    if (!correctId) {
      return res.status(400).json({message: 'Id is not correct'});
    }
    next();
  } catch (err) {
    res.status(500).json({message: 'Server error'});
    console.log(err);
  }
};

module.exports = {
  idValidator,
};
