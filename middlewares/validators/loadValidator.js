const Joi = require('joi');
const Load = require('../../models/load');

const loadSchema = Joi.object().keys({
  name: Joi.string().required(),
  payload: Joi.number().required(),
  pickup_address: Joi.string().required(),
  delivery_address: Joi.string().required(),
  dimensions: Joi.object().keys({
    width: Joi.number().required(),
    length: Joi.number().required(),
    height: Joi.number().required(),
  }).required(),
});

const loadStatusSchema = Joi.string()
    .valid('NEW', 'POSTED', 'ASSIGNED', 'SHIPPED');

const loadValidator = async (req, res, next) => {
  const data = req.body;
  try {
    await loadSchema.validateAsync(data);
    next();
  } catch (err) {
    const message = err.details[0].message;
    res.status(400).json({message});
  }
};

const loadStatusValidator = async (req, res, next) => {
  const {status} = req.query;
  try {
    await loadStatusSchema.validateAsync(status);
    next();
  } catch (err) {
    const message = err.details[0].message;
    res.status(400).json({message});
  }
};

const isLoadExist = async (req, res, next) => {
  const userId = req.decoded._id;
  const {id} = req.params;
  try {
    const load = await Load.getLoad(id, userId);
    if (!load) {
      return res.status(400).json({message: 'Load not found'});
    }
    next();
  } catch (err) {
    res.status(500).json({message: 'Server error'});
    console.log(err);
  }
};

const isLoadNew = async (req, res, next) => {
  const userId = req.decoded._id;
  const {id} = req.params;
  try {
    const load = await Load.getNewLoad(id, userId);
    if (!load) {
      return res.status(400).json({
        message: 'You can do this only with new load',
      });
    }
    next();
  } catch (err) {
    res.status(500).json({message: 'Server error'});
    console.log(err);
  }
};

const isLoadActive = async (req, res, next) => {
  const userId = req.decoded._id;
  const {id} = req.params;
  try {
    const load = await Load.getLoad(id, userId);
    if (load.status !== 'ASSIGNED' && load.status !== 'POSTED') {
      return res.status(400).json({
        message: 'You can do this only with active load',
      });
    }
    next();
  } catch (err) {
    res.status(500).json({message: 'Server error'});
    console.log(err);
  }
};

module.exports = {
  loadValidator,
  loadStatusValidator,
  isLoadExist,
  isLoadNew,
  isLoadActive,
};
