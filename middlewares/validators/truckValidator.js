const Joi = require('joi');
const Truck = require('../../models/truck');

const truckTypeSchema = Joi.object().keys({
  type: Joi.string()
      .valid('SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT')
      .required(),
});

const truckValidator = async (req, res, next) => {
  const data = req.body;
  try {
    await truckTypeSchema.validateAsync(data);
    next();
  } catch (err) {
    const message = err.details[0].message;
    res.status(400).json({message});
  }
};

const isTruckExist = async (req, res, next) => {
  const userId = req.decoded._id;
  const {id} = req.params;
  try {
    const truck = await Truck.getTruck(id, userId);
    if (!truck) {
      return res.status(400).json({message: 'Truck not found'});
    }
    next();
  } catch (err) {
    res.status(500).json({message: 'Server error'});
    console.log(err);
  }
};

const isTruckAssigned = async (req, res, next) => {
  const userId = req.decoded._id;
  const {id} = req.params;
  try {
    const truck = await Truck.getNotAssignedTruck(id, userId);
    if (!truck) {
      return res.status(400).json({
        message: 'You can\'t do this with assigned truck',
      });
    }
    next();
  } catch (err) {
    res.status(500).json({message: 'Server error'});
    console.log(err);
  }
};

module.exports = {
  truckValidator,
  isTruckExist,
  isTruckAssigned,
};
