const Joi = require('joi');

const changePasswordSchema = Joi.object().keys({
  oldPassword: Joi.string().required(),
  newPassword: Joi.string().required(),
});

const userValidator = async (req, res, next) => {
  const data = req.body;
  try {
    await changePasswordSchema.validateAsync(data);
    next();
  } catch (err) {
    const message = err.details[0].message;
    res.status(400).json({message});
  }
};

module.exports = {
  userValidator,
};
