const {Schema, model} = require('mongoose');
const Log = require('../models/log');

const loadSchema = new Schema({
  created_by: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
    default: '',
  },
  status: {
    type: String,
    default: 'NEW',
  },
  state: {
    type: String,
    default: '',
  },
  name: {
    type: String,
    required: true,
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  dimensions: {
    width: {
      type: Number,
      required: true,
    },
    length: {
      type: Number,
      required: true,
    },
    height: {
      type: Number,
      required: true,
    },
  },
  logs: [Log.schema],
  created_date: {
    type: Date,
    default: Date.now,
  },
}, {versionKey: false});

const Load = module.exports = model('Load', loadSchema);

module.exports.saveLoad = (load) => load.save();

module.exports.getLoad = (_id, userId) => {
  return Load.findOne({_id, created_by: userId});
};

module.exports.getActiveLoad = (userId) => {
  return Load.findOne({
    $or: [{status: 'ASSIGNED'}, {status: 'POSTED'}],
    $and: [{assigned_to: userId}],
  });
};

module.exports.getNewLoad = (_id, userId) => {
  const filter = {_id, created_by: userId, status: 'NEW'};
  return Load.findOne(filter);
};

module.exports.updateLoad = (_id, userId, role, updateData) => {
  let filter;
  if (role === 'DRIVER') {
    filter = {_id, assigned_to: userId};
  }
  if (role === 'SHIPPER') {
    filter = {_id, created_by: userId};
  }
  return Load.findOneAndUpdate(filter, updateData);
};

module.exports.deleteLoad = (_id, userId) => {
  const filter = {_id, created_by: userId};
  return Load.findOneAndDelete(filter);
};

module.exports.deleteUsersLoads = (userId) => {
  return Load.deleteMany({created_by: userId});
};

module.exports.getLoadsByFilter = (userId, role, status, limit, offset) => {
  const filter = {};
  if (status) {
    filter.status = status;
  }
  if (role === 'DRIVER') {
    filter.assigned_to = userId;
  }
  if (role === 'SHIPPER') {
    filter.created_by = userId;
  }
  return Load.find(filter).skip(offset).limit(limit);
};
