const {Schema, model} = require('mongoose');

const logSchema = new Schema({
  message: {
    type: String,
    required: true,
  },
  time: {
    type: Date,
    default: Date.now,
  },
}, {_id: false});

module.exports = model('Log', logSchema);

module.exports.addLog = (newLog, load) => {
  return load.updateOne({$push: {logs: newLog}});
};

module.exports.deleteLog = (_id, load) => {
  return load.updateOne({$pull: {logs: {_id}}});
};
