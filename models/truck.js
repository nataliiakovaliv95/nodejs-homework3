const {Schema, model} = require('mongoose');

const truckSchema = new Schema({
  created_by: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
    default: '',
  },
  type: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    default: 'IS',
  },
  created_date: {
    type: Date,
    default: Date.now,
  },
}, {versionKey: false});

const Truck = module.exports = model('Truck', truckSchema);

module.exports.getTrucks = (userId) => Truck.find({created_by: userId});

module.exports.saveTruck = (truck) => truck.save();

module.exports.getTruck = (_id, userId) => {
  return Truck.findOne({_id, created_by: userId});
};

module.exports.getAssignedTruck = (userId) => {
  return Truck.findOne({assigned_to: userId});
};

module.exports.getNotAssignedTruck = (_id, userId) => {
  const filter = {_id, created_by: userId, assigned_to: ''};
  return Truck.findOne(filter);
};

module.exports.updateTruck = (_id, userId, type) => {
  const filter = {_id, created_by: userId};
  return Truck.findOneAndUpdate(filter, {type});
};

module.exports.deleteTruck = (_id, userId) => {
  const filter = {_id, created_by: userId};
  return Truck.findOneAndDelete(filter);
};

module.exports.assignTruck = (_id, userId) => {
  const filter = {_id, created_by: userId};
  return Truck.findOneAndUpdate(filter, {assigned_to: userId});
};

module.exports.deleteUsersTrucks = (userId) => {
  return Truck.deleteMany({created_by: userId});
};

module.exports.updateTruckExceptSelected = (_id, userId) => {
  const filter = {
    _id: {$nin: _id},
    created_by: userId,
    assigned_to: userId,
  };
  return Truck.findOneAndUpdate(filter, {assigned_to: ''});
};

module.exports.getFreeTrucks = () => {
  return Truck.find({assigned_to: {$ne: ''}, status: 'IS'});
};

module.exports.updateTruckStatus = (userId, status) => {
  return Truck.findOneAndUpdate({assigned_to: userId}, {status});
};
