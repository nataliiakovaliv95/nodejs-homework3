const {Schema, model} = require('mongoose');

const userSchema = new Schema({
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  role: {
    type: String,
    required: true,
  },
  created_date: {
    type: Date,
    default: Date.now,
  },
}, {versionKey: false});

const User = module.exports = model('User', userSchema);

module.exports.getUserByEmail = (email) => {
  return User.findOne({email});
};

module.exports.addUser = (user) => user.save();

module.exports.deleteUser = (id) => User.findByIdAndRemove(id);

module.exports.changeUserPassword = (_id, password) => {
  return User.findByIdAndUpdate({_id}, {password});
};
