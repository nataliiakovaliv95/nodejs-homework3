const express = require('express');
const router = new express.Router();
const {checkPassword} = require('../middlewares/checkPasswordMiddleware');
const {
  authValidator,
  isUserExist,
} = require('../middlewares/validators/authValidator');
const {
  registerUser,
  loginUser,
  resetUsersPassword,
} = require('../controllers/authController');


router.post('/register', authValidator, isUserExist, registerUser);

router.post('/login', authValidator, isUserExist, checkPassword, loginUser);

router.post(
    '/forgot_password', authValidator, isUserExist, resetUsersPassword,
);


module.exports = router;
