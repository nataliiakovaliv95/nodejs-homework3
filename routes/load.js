const express = require('express');
const router = new express.Router();
const {verifyToken} = require('../middlewares/tokenMiddleware');
const {isDriver, isShipper} = require('../middlewares/checkRoleMiddleware');
const {idValidator} = require('../middlewares/validators/idValidator');
const {
  loadValidator,
  loadStatusValidator,
  isLoadExist,
  isLoadNew,
  isLoadActive,
} = require('../middlewares/validators/loadValidator');
const {
  getLoads,
  addLoad,
  getActiveLoad,
  updateLoadState,
  getLoad,
  updateLoad,
  deleteLoad,
  postLoad,
  getLoadShippingInfo,
} = require('../controllers/loadController');

router.get('/', verifyToken, loadStatusValidator, getLoads);

router.post('/', verifyToken, isShipper, loadValidator, addLoad);

router.get('/active', verifyToken, isDriver, getActiveLoad);

router.patch('/active/state', verifyToken, isDriver, updateLoadState);

router.get('/:id', verifyToken, idValidator, isLoadExist, getLoad);

router.put(
    '/:id', verifyToken, isShipper, idValidator,
    loadValidator, isLoadExist, isLoadNew, updateLoad,
);

router.delete(
    '/:id', verifyToken, isShipper, idValidator,
    isLoadExist, isLoadNew, deleteLoad,
);

router.post(
    '/:id/post', verifyToken, isShipper, idValidator,
    isLoadExist, isLoadNew, postLoad,
);

router.get(
    '/:id/shipping_info', verifyToken, isShipper,
    idValidator, isLoadExist, isLoadActive, getLoadShippingInfo,
);


module.exports = router;
