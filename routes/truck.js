const express = require('express');
const router = new express.Router();
const {verifyToken} = require('../middlewares/tokenMiddleware');
const {isDriver} = require('../middlewares/checkRoleMiddleware');
const {idValidator} = require('../middlewares/validators/idValidator');
const {
  truckValidator,
  isTruckExist,
  isTruckAssigned,
} = require('../middlewares/validators/truckValidator');
const {
  getTrucks,
  addTruck,
  getTruck,
  updateTruck,
  deleteTruck,
  assignTruck,
} = require('../controllers/truckController');


router.get('/', verifyToken, isDriver, getTrucks);

router.post('/', verifyToken, isDriver, truckValidator, addTruck);

router.get(
    '/:id', verifyToken, isDriver,
    idValidator, isTruckExist, getTruck,
);

router.put(
    '/:id', verifyToken, isDriver, idValidator,
    truckValidator, isTruckExist, isTruckAssigned, updateTruck,
);

router.delete(
    '/:id', verifyToken, isDriver,
    idValidator, isTruckExist, isTruckAssigned, deleteTruck,
);

router.post(
    '/:id/assign', verifyToken, isDriver,
    idValidator, isTruckExist, isTruckAssigned, assignTruck,
);


module.exports = router;
