const express = require('express');
const router = new express.Router();
const {verifyToken} = require('../middlewares/tokenMiddleware');
const {userValidator} = require('../middlewares/validators/userValidator');
const {checkPassword} = require('../middlewares/checkPasswordMiddleware');
const {
  getUserInfo,
  deleteUserProfile,
  changeUserPassword,
} = require('../controllers/userController');


router.get('/', verifyToken, getUserInfo);

router.delete('/', verifyToken, deleteUserProfile);

router.patch(
    '/password', verifyToken, userValidator, checkPassword, changeUserPassword,
);


module.exports = router;
