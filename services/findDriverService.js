const {truckTypes} = require('../static/truckTypes');
const Truck = require('../models/truck');

const findFreeDriver = async (load) => {
  const freeTrucks = await Truck.getFreeTrucks();
  const freeDriver = freeTrucks.find((truck) => {
    const type = truck.type;
    return load.payload <= truckTypes[type].payload &&
    load.dimensions.width <= truckTypes[type].dimensions.width &&
    load.dimensions.length <= truckTypes[type].dimensions.length &&
    load.dimensions.height <= truckTypes[type].dimensions.height;
  });
  return freeDriver;
};

module.exports = {
  findFreeDriver,
};
