const {loadStates} = require('../static/loadStates');

const getNextLoadState = (currentState) => {
  const index = loadStates.findIndex((state) => state === currentState);
  const nextState = loadStates[index + 1];
  return nextState;
};

module.exports = {
  getNextLoadState,
};
