const Log = require('../models/log');

const addNewLog = async (load, message) => {
  const newLog = new Log({message});
  await Log.addLog(newLog, load);
};

module.exports = {
  addNewLog,
};
